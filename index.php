<?php
// echo '<pre>';
// print_r($_SERVER);
// echo '</pre>';
// exit;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


try {
    if ($_SERVER['SERVER_NAME'] == 'localhost') {
        $pdo = new PDO('mysql:host=localhost;dbname=shareposts', 'root', 'password');
    } else {
        $pdo = new PDO('mysql:host=172.30.196.14;db=shareposts', 'root', 'password');
    }
    
    echo 'Connected <br>';
} catch (PDOException $e) {
    echo 'Error connecting to DB: ' . $e->getMessage();
}

if (isset($_GET['p']) && $_GET['p'] == 'display') {
    $prep = $pdo->prepare("SHOW TABLES");
    // $prep = $pdo->prepare("SELECT * FROM test1 WHERE 1");
    $prep->execute();
    $res = $prep->fetchAll(PDO::FETCH_ASSOC);
    print_r($res);
    echo '<br><br>';
}

if(isset($_GET['p']) && $_GET['p'] == 'describe') {
    $prep = $pdo->prepare("DESCRIBE test1");
    $prep->execute();
    $res = $prep->fetchAll();
    print_r($res);
}

if(isset($_GET['p']) && $_GET['p'] == 'create') {
    echo 'CREATING TABLE: <br>';
    $prep = $pdo->prepare("CREATE TABLE test1 (id INT(5), content VARCHAR(255), name VARCHAR(10)) ");
    print_r($prep->execute());
    
}

if(isset($_GET['p']) && $_GET['p'] == 'insert') {
    echo 'INSERTING: <br>';
    $prep = $pdo->prepare("INSERT INTO test1 (id, content, name) VALUES (1, 'SOME TEST CONTENT', 'Jim Bob')");
    $e = $prep->execute();
    print_r($e);
}

if(isset($_GET['p']) && $_GET['p'] == 'drop') {
    echo 'DROPING TABLE: <br>';
    $prep = $pdo->prepare("DROP TABLE test1");
    $e = $prep->execute();
    print_r($e);
}
?>